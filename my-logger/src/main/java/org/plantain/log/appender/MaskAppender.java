package org.plantain.log.appender;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.appender.AppenderLoggingException;

import java.io.Serializable;
import java.util.concurrent.locks.*;
import org.apache.logging.log4j.core.*;
import org.apache.logging.log4j.core.config.Property;
import org.apache.logging.log4j.core.config.plugins.*;
import org.apache.logging.log4j.core.layout.PatternLayout;

@Plugin(name = "MaskAppender", category = "Core", elementType = "appender", printObject = true)
public class MaskAppender extends AbstractAppender {

	private final ReadWriteLock rwLock = new ReentrantReadWriteLock();
	private final Lock readLock = rwLock.readLock();
	private String[] masks;
	private MaskUtils masker;

	protected MaskAppender(String name, Filter filter, Layout<? extends Serializable> layout,
			final boolean ignoreExceptions, Property[] properties) {
		super(name, filter, layout, ignoreExceptions, properties);
	}
	
	protected MaskAppender(String name, Filter filter, Layout<? extends Serializable> layout,
			final boolean ignoreExceptions, Property[] properties, String[] mymasks ) {
		super(name, filter, layout, ignoreExceptions, properties);
		this.masks = mymasks;
		this.masker = new MaskUtils(this.masks);
		System.out.println("--- Mask Appender with keys ---");
	}
	

	@Override
	public void append(LogEvent event) {
		readLock.lock();
		try {
			final byte[] bytes = getLayout().toByteArray(event);
			String val = new String(bytes);
			System.out.println(masker.maskValue(val));
		} catch (Exception ex) {
			if (!ignoreExceptions()) {
				throw new AppenderLoggingException(ex);
			}
		} finally {
			readLock.unlock();
		}
	}

	@PluginFactory
	public static MaskAppender createAppender(@PluginAttribute("name") String name,
			@PluginElement("Layout") Layout<? extends Serializable> layout,
			@PluginElement("Filter") final Filter filter, 
			@PluginAttribute("otherAttribute") String otherAttribute) {
		
		if (name == null) {
			LOGGER.error("No name provided for MyCustomAppenderImpl");
			return null;
		}
		
		if (layout == null) {
			layout = PatternLayout.createDefaultLayout();
		}
		
		if (otherAttribute == null) {
			System.out.println("otherAttribute == null");
		} else {
			String[] emasks = otherAttribute.split(",");
			System.out.println("Masks: "+ emasks.length +" words");
			return new MaskAppender(name, filter, layout, true, null, emasks);
		}
		return new MaskAppender(name, filter, layout, true, null);
	}

}
