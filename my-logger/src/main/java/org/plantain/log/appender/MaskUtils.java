package org.plantain.log.appender;

import org.apache.commons.lang.StringUtils;

/**
 * MaskUtils
 * 
 * @author plantain
 * 
 * csn:123456 -> csn:******
 * 
 */
public class MaskUtils {

	private String[] keys;
	//private final String keydelimiter = ":";
	
	
	public MaskUtils(String[] mykeys) {
		this.keys = mykeys;
		
	}
	
	public String maskValue(String message) {
		String log = message;
		String[] values = log.split(":");
		String result = "";
		
		for (String key : this.keys) {
			if (values[0].contains(key)) {
				String masked = StringUtils.repeat("*", values[1].length());
				result = result.concat(values[0]).concat(":").concat(masked);
				return result;
			} 
		}
		
		return message;
	}
	
}
