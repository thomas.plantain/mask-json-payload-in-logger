package org.plantain.logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Bar {
	
	static final Logger logger = LogManager.getLogger(Bar.class.getName());
	 
	  public boolean doIt() {
	    logger.error("phone:0771225657");
	    logger.error("csn:123456");
	    logger.error("ephone:0771225657");
	    logger.error("niss:123456");
	    return false;
	  }
	  
	  

}
