package org.plantain.logger;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class TestLogger {

	private static final Logger logger = LogManager.getLogger(TestLogger.class);
	 
    public static void main(final String... args) {
        logger.trace("Entering application.");
        Bar bar = new Bar();
        bar.doIt();
        logger.trace("Exiting application.");
        
    }

}
