# mask json payload in logger

Mask information in the json payload logged

## Getting started

- https://logging.apache.org/log4j/2.x/manual/appenders.html
- https://www.baeldung.com/log4j2-custom-appender
- https://facingissuesonit.com/2018/05/23/how-to-mask-json-confidential-personal-information-in-logs-java/
- https://stackoverflow.com/questions/29263036/json-pii-data-masking-in-java
